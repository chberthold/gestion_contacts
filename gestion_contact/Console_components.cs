﻿using gestion_contact_data;
using gestion_contact_serialisation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace gestion_contact
{
    /*****************************************************
     * Class Console_components                          *
     * Regroups all the methodes used by the console     *
     * application                                       *
     *****************************************************/
    public static class Console_components
    {
        private static readonly Serialisation_manager serialize_manager = new Serialisation_manager();

        /// <summary>
        /// Method GetTree :
        /// Retrieves a tree from a file
        /// </summary>
        /// <param name="root">Current root of the user's tree</param>
        /// <param name="current">Folder of the user's tree which is the current</param>
        public static void GetTree(ref Dossier root, ref Dossier current)
        {
            string name;
            bool encrypt;
            string key = "";
            Dossier test = null;
            string confirm;

            Console.WriteLine("Votre arbre de dossiers actuel sera supprimer, pensez à le sauvegarder");
            Console.WriteLine("Continuer ? (y/n)");
            confirm = Console.ReadLine();

            /* If the user whishes to still load a tree */
            if (confirm == "y")
            {

                Console.WriteLine("Le chemin standard est {0})", AbsolutePath.DirectPath);
                Console.WriteLine("Saisir le nom du fichier :");
                name = Console.ReadLine();
                encrypt = name.Contains("_encrypted");

                /* If the file is encrypted, get the key */
                if (encrypt)
                {
                    Console.WriteLine("Le fichier est chiffré, veuillez saisir la clé de déchiffrement : ");
                    key = Console.ReadLine();
                    test = serialize_manager.Deserialize(name, true, key);
                }

                else
                {
                    test = serialize_manager.Deserialize(name, false, key);
                }

                /* If the deserialization was a success */
                if (test != null)
                {
                    root = test;
                    current = test;
                    Console.WriteLine("Récupération terminée !\n");
                }

                else
                {
                    Console.WriteLine("Abandon du chargement...\n");
                }
            }

            else
            {
                Console.WriteLine("Abandon du chargement...\n");
            }

        }

        /// <summary>
        /// Method SaveTree :
        /// Saves a tree of folders in a file
        /// </summary>
        /// <param name="root">Root folder of the tree to serialize</param>
        public static void SaveTree(Dossier root)
        {
            string name;
            string[] split;
            string encrypt;
            string key = "";

            Console.WriteLine("Les dossiers seront enregistrés dans le dossier {0}.", AbsolutePath.DirectPath);
            Console.WriteLine("Saisir un nom pour le fichier : (.bin ou .xml)");
            name = Console.ReadLine();
            split = name.Split('.');
            Console.WriteLine("Souhaitez-vous chiffrer le fichier ? (y/n)");
            encrypt = Console.ReadLine();

            /* If the user wishes to encrypt the file */
            if (encrypt == "y")
            {
                name = string.Join(".", split.Take(split.Count() - 1)) + "_encrypted." + split.Last();
                Console.WriteLine("Saisir un mot de passe pour le chiffrement (il vous sera redemandé au chargement du fichier) :");
                key = Console.ReadLine();
                serialize_manager.Serialize(root, name, true, key);
            }
            else
            {  
                /* The pattern '_encrypted' is only for encrypted files */
                if (!name.Contains("_encrypted"))
                {
                    serialize_manager.Serialize(root, name, false, key);
                }
            }

        }

        /// <summary>
        /// Method AddContact :
        /// Adds an individual to the children of the given folder
        /// </summary>
        /// <param name="current">The folder in which to add the contact</param>
        public static void AddContact(Dossier current)
        {
            string nom;
            string prenom;
            string societe;
            string courriel;
            string relation;
            Relation lien = Relation.Ami;
            bool ok = false;
            Contact next = null;

            /* Get all the information needed */
            Console.WriteLine("\n\nLe contact sera ajouté au dossier courant (" + current.GetPath("") + ")");
            Console.WriteLine("Saisir le nom du contact : ");
            nom = Console.ReadLine();
            Console.WriteLine("Saisir le prénom du contact : ");
            prenom = Console.ReadLine();
            Console.WriteLine("Saisir le courriel du contact : (------@-------.--)");
            courriel = Console.ReadLine();

            /* Tant que le mail n'est pas dans le bon format */
            while (!Regex.IsMatch(courriel, @"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$"))
            {
                Console.WriteLine("Format de mail incorrect, veuillez respecter le format example@example.example");
                courriel = Console.ReadLine();
            }
            Console.WriteLine("Saisir la société du contact : ");
            societe = Console.ReadLine();

            /* The user can only choose one of the four options */
            do
            {
                Console.WriteLine("Relation avec le contact : \n\n1- Ami\n2- Collègue\n3- Relation\n4- Réseau");
                relation = Console.ReadLine();
                switch (relation)
                {
                    case "1":
                    case "Ami":
                        ok = true;
                        break;
                    case "2":
                    case "Collègue":
                        lien = Relation.Collegue;
                        ok = true;
                        break;
                    case "3":
                    case "Relation":
                        lien = Relation.Relation;
                        ok = true;
                        break;
                    case "4":
                    case "Réseau":
                        lien = Relation.Reseau;
                        ok = true;
                        break;
                    default:
                        Console.WriteLine("Option inexistante !\n");
                        break;

                }
            } while (!ok);


            Console.WriteLine("\nCréation du contact ... \n");
            next = new Contact(nom, prenom, courriel, societe, lien);
            current.AddFichier(next);

            Console.WriteLine("Done !\n");
        }

        /// <summary>
        /// Method AddDossier :
        /// Adds a folder in the list of children of the given folder
        /// </summary>
        /// <param name="current">The folder in which to add the new folder</param>
        public static void AddDossier(ref Dossier current)
        {
            string nom;
            string confirm;
            Dossier next = null;
            Fichier exists;

            Console.WriteLine("\n\nLe dossier sera ajouté au dossier courant (" + current.GetPath("") + ")");
            Console.WriteLine("Saisir un nom pour le nouveau dossier : ");
            nom = Console.ReadLine();
            Console.WriteLine("\nLe dossier se nommera " + nom + ". Confirmer ? (y/n)");
            confirm = Console.ReadLine();

            /* If the user is satisfied with the name of the folder */
            if (confirm.Equals("y"))
            {
                exists = current.Find(nom);

                /* If there isn't already a folder with the same name in the list of children of the given folder */
                if (exists == null || exists is Contact)
                {
                    Console.WriteLine("\nCréation du fichier ... \n");
                    next = new Dossier(nom, current);
                    current.AddFichier(next);
                    current = next;
                }

                else
                {
                    Console.WriteLine("Le dossier " + nom + " existe déjà.\nCréation annulée ...\n");
                }
            }
            else
            {
                Console.WriteLine("\nCréation annulée ...\n");
            }

            Console.WriteLine("Done !\n");
        }


        /// <summary>
        /// Method Tree :
        /// Displays the tree from the file given
        /// </summary>
        /// <param name="current">The file to display in the tree</param>
        /// <param name="level">Which level this file is in the tree</param>
        public static void Tree(Fichier current, int level)
        {
            /* Display the name of the file */
            for (int i = 0; i < level; i++)
            {
                Console.Write("  ");
            }
            Console.WriteLine("|");

            for (int i = 0; i < level; i++)
            {
                Console.Write("  ");
            }
            Console.Write(" -- ");

            Console.WriteLine(current.GetNom());

            /* If the file is a repository */
            if (current is Dossier)
            {

                /* Display all the files in the repository */
                foreach (Fichier f in ((Dossier)current).Fichiers)
                {
                    Tree(f, level + 1);
                }
            }

        }

        /// <summary>
        /// Method ChangeDirectory :
        /// Move up or down one level in the tree
        /// </summary>
        /// <param name="cur">The current folder</param>
        public static void ChangeDirectory(ref Dossier cur)
        {
            string nom;
            Fichier down;

            Console.WriteLine("\nArbre courant :\n" + cur.getDossiers() + "\n");
            Console.WriteLine("Saisir un nom de dossier dans le dossier courant ou .. pour revenir dans le dossier parent\n");
            nom = Console.ReadLine();

            /* If the user wishes to go up on level */
            if (nom == "..")
            {
                /* If the current folder is not the root */
                if (cur.Parent != null)
                {
                    cur = cur.Parent;
                }
                else
                {
                    Console.WriteLine("Ce dossier est la racine\n");
                }
            }

            /* If the user wishes to go down one level */
            else
            {
                down = cur.Find(nom);

                /* If there is a folder with the given name */
                if (down != null && down is Dossier)
                {
                    cur = (Dossier)down;
                    Console.WriteLine("Dossier trouvé, déplacement dans le dossier.\n");
                }

                else
                {
                    Console.WriteLine("Dossier inconnu\n");
                }
            }
        }


        /// <summary>
        /// Method Menu :
        /// Displays all the options to the user
        /// </summary>
        /// <param name="cur">The directory in which the user currently is</param>
        public static void Menu(Dossier cur)
        {
            string currentPath = cur.GetPath("");

            Console.WriteLine("\t ----------------------------");
            Console.WriteLine("\t|                            |");
            Console.WriteLine("\t|    Choisir une action :    |");
            Console.WriteLine("\t|                            |");
            Console.WriteLine("\t ----------------------------\n");
            Console.WriteLine("\t*Dossier courrant : " + currentPath + "*\n\n");
            Console.WriteLine("\t Afficher (tree)");
            Console.WriteLine("\t Changer de dossier (cd)");
            Console.WriteLine("\t Charger (load)");
            Console.WriteLine("\t Enregister (save)");
            Console.WriteLine("\t Ajouter un dossier (mkdir)");
            Console.WriteLine("\t Ajouter un contact (mkcon)");
            Console.WriteLine("\t Sortir (exit)\n");
        }
    }
}
