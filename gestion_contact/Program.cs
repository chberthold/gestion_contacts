﻿using System;
using gestion_contact_data;

namespace gestion_contact
{
    /***********************************
     * Main program                    *
     ***********************************/
    public class Program
    {
        static void Main(string[] args)
        {
            Dossier Racine = new Dossier("home"); // Provide an initial folder for the user
            Dossier Current = Racine;
            string answer;
            bool Out = false;

            /* While the user doesn't want to exit */
            while (!Out)
            {
                Console_components.Menu(Current); // Display the options
                answer = Console.ReadLine();

                switch (answer)
                {
                    case "Afficher":
                    case "tree":
                        Console_components.Tree(Current, 0); // Display the tree starting from the current folder
                        Console.WriteLine("\n");
                        break;
                    case "Charger":
                    case "load":
                        Console_components.GetTree(ref Racine, ref Current); // Retreive a tree stored in a file
                        break;
                    case "Enregistrer":
                    case "save":
                        Console_components.SaveTree(Racine); // Save the current tree from the root in a file
                        break;
                    case "Ajouter un dossier":
                    case "mkdir":
                        Console_components.AddDossier(ref Current); /* Add a new folder to the children of the current one  *
                                                                     * The new folder becomes the current folder            */
                        break;
                    case "Ajouter un contact":
                    case "mkcon":
                        Console_components.AddContact(Current); // Add a new contact to the children of the current folder
                        break;
                    case "Changer de dossier":
                    case "cd":
                        Console_components.ChangeDirectory(ref Current); // Move up or down in the tree
                        break;
                    case "Sortir":
                    case "exit":
                        Out = true; // Exit the app
                        break;
                    default:
                        Console.WriteLine("Opération inconnue !\n");
                        break;

                }
            }
        }
    }
}
